AndroidVRPNGraph
================

A VRPN client for android which displays data graphically.

Utilizes [VRPN](http://www.cs.unc.edu/Research/vrpn/) and [AChartEngine](http://www.achartengine.org/).

For information on compiling VRPN for Android using [android-cmake](https://code.google.com/p/android-cmake/), refer to this [mailing list thread](http://lists.unc.edu/read/messages?id=6391483).


Unpublished Work Copyright (c) 2012

Primary Developer:
Andrew Montag <ajmontag@gmail.com>

Project Owners:
Kevin Godby <kevin@godby.org>
Nir Keren <nir@iastate.edu>
