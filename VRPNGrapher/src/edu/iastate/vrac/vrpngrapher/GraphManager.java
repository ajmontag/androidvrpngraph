package edu.iastate.vrac.vrpngrapher;

import java.util.ArrayList;
import java.util.List;

import edu.iastate.vrac.vrpngrapher.graphs.ALiveChart;
import edu.iastate.vrac.vrpngrapher.graphs.ILiveChart;
import edu.iastate.vrac.vrpngrapher.model.GraphModel;

import vrpn.VRPNDevice;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class GraphManager {
	
	ArrayAdapter<ILiveChart> arr_;
	Activity activity_; 
	
	public GraphManager(Activity activity) {
		activity_ = activity; 
		arr_ = new ArrayAdapter<ILiveChart>(activity_, android.R.layout.simple_list_item_1, GraphModel.getGraphList());
		GraphModel.addListener(arr_);
	}
	
	public void promptNewGraph(VRPNDevice device) {
		new NewGraphWizard(activity_, device); 	    
	}
	
	public ArrayAdapter<ILiveChart> getListAdapter() {
		return arr_; 
	}
	
	// determines which graphs are eligable for which devices
	private static String[] getCompatibleGraphTypeNames(Class<? extends VRPNDevice> class1) {
		ArrayList<String> list = new ArrayList<String>(); 
		list.add("TrigonometricFunctionsChart"); 
		list.add("AnalogLineGraph"); 
		list.add("GraphType3"); 
		list.add("GraphType4"); 
		return list.toArray(new String[0]);
	}
	
	// inner classes lie below

	private class NewGraphWizard {
		
		Dialog dialog_; 
		
		public NewGraphWizard(final Activity activity, final VRPNDevice device) {
			// TODO Auto-generated constructor stub
			final String[] list = GraphManager.getCompatibleGraphTypeNames(device.getClass()); 
			
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		    // Set the dialog title
		    builder.setTitle("New VRPN Connection")
		    // Set the action buttons
	           .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	                   NewGraphWizard.this.dialog_.cancel(); 
	               }
	           })
	           .setItems(list, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	                   // TODO 
	            	   // list[which] is the type of graph you want to create
	            	   String graphName = device.getConnectionName() + " | " + list[which]; 
	            	   GraphModel.addGraph(graphName, list[which], device);
	               }
	           });
		    
		    
		    dialog_ = builder.create();
		    dialog_.show(); 
		}
	}

	public ILiveChart getGraph(int position) {
		return arr_.getItem(position);
	}
}

