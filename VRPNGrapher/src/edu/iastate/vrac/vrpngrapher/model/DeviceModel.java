package edu.iastate.vrac.vrpngrapher.model;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import vrpn.VRPNDevice;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import edu.iastate.vrac.vrpngrapher.VRPNRemoteFactory;
import edu.iastate.vrac.vrpngrapher.model.JSONModel.JSONConnectionPair;

public class DeviceModel {

	private static List<VRPNDevice> deviceList = new ArrayList<VRPNDevice>(); 
	private static Set<BaseAdapter> listeners = new HashSet<BaseAdapter>(); 
	
	public static List<VRPNDevice> getDeviceList() {
		return deviceList;
	}
	
	public static VRPNDevice getDevice(String name) {
		for (VRPNDevice dev : deviceList) {
			if (dev.toString().equals(name))
				return dev;
		}
		return null;
	}

	public static void addListener(BaseAdapter listener) {
		if (listener != null)
			listeners.add(listener);
	}
	
	private static void notifyListeners() {
		for (BaseAdapter listener : listeners) {
			listener.notifyDataSetChanged();
		}
	}
	
	///// JSON stuff

	public static List<JSONConnectionPair> getJsonArray() {
		ArrayList<JSONConnectionPair> al = new ArrayList<JSONConnectionPair>(deviceList.size()); 
		for (VRPNDevice dev : deviceList) {
			al.add(new JSONConnectionPair(dev.toString(), dev.getClass().getSimpleName())); 
		}
		return al;
	}
	
	public static void connectAndAdd(List<JSONConnectionPair> list, boolean block) {
		for (JSONConnectionPair p : list) {
			// XXX not sure if this will work without a delay in there during the async tasks. should...
			connectAndAdd(p.address, p.deviceType, block); 
		}
	}
	
	public static void connectAndAdd(String address, String deviceType, boolean block) {
	 	   OpenVRPNConnectionTask connectTask = new OpenVRPNConnectionTask();
	 	   String[] params = new String[2];
	 	   params[0] = deviceType; 
	 	   params[1] = address;  
	 	   if (!block) {
	 		   connectTask.execute(params);
	 	   } else {
	 		   connectTask.onPostExecute(connectTask.doInBackground(params));
	 		   notifyListeners();
	 	   }
	}
	
	// inner classes lie below
	
	private static class OpenVRPNConnectionTask extends AsyncTask<String, Boolean, Pair<VRPNDevice, Exception>> {

		public OpenVRPNConnectionTask() {
			
		}
		
		/** 
		 * @param params params[0]:=vrpnType, params[1]:=location (name)
		 */
		@Override
		protected Pair<VRPNDevice, Exception> doInBackground(String... params) {
			VRPNDevice dev = null; 
			Exception except = null;

			try {
				Log.v("OpenVRPNConnectionTask", "Opening device [" + params[0] + "][" + params[1] + "]"); 
				dev = VRPNRemoteFactory.fromTypeName(params[0], params[1], "", "", "", ""); 
				if (!dev.isConnected()) {
					except =  new ConnectException("Unable to Connect to \"" + params[1] + "\""); 
				}
			} catch (Exception e) {
				except = e; 
			}
			Log.v("OpenVRPNConnectionTask", "Opened  device [" + params[0] + "][" + params[1] + "]"); 
			return new Pair<VRPNDevice, Exception>(dev, except);
		}

		@Override
		protected void onPostExecute(Pair<VRPNDevice, Exception> result) {
			Log.v("connect task", "onPostExecute");
			this.publishProgress(true);
			// show result in the UI
			if (result != null && result.second == null) {
				deviceList.add(result.first);
				Log.i("DeviceModel", "Connection Successful: " + result.first); 
				notifyListeners();
				//Toast.makeText(activity_, "Connection Successful", Toast.LENGTH_SHORT).show();
			} else {
				if (result == null) {
					Log.w("DeviceModel", "Null Return Pair");
				} else {
					Log.w("OpenVRPNCOnnectionTask", "Exception:" + result.second); 
					//Toast.makeText(activity_, "Connection Failure [" + result.second.toString() + "]", Toast.LENGTH_LONG).show();
				}
			}
			Log.v("connect task", "onPostExecute exit");
		}
	}
		
}
