package edu.iastate.vrac.vrpngrapher.model;

import java.util.List;

import edu.iastate.vrac.vrpngrapher.GraphManager;
import edu.iastate.vrac.vrpngrapher.MainActivity;

public class JSONModel {
	
	public List<JSONGraphPair> graphs;
	public List<JSONConnectionPair> connections; 

	public JSONModel() { /* do nothing */ }
	
	// construct the JSON model from the models
	public JSONModel(MainActivity ma) {
		graphs = GraphModel.getJsonArray();
		connections = DeviceModel.getJsonArray();  
	}
	
	// inner classes 
	
	public static class JSONConnectionPair {
		String address; 
		String deviceType; 
		public JSONConnectionPair() { /* do nothing */ }
		public JSONConnectionPair(String address, String deviceType) {
			this.address = address; 
			this.deviceType = deviceType; 
		}
	}
	
	public static class JSONGraphPair {
		String graphName; 
		String graphType; 
		String deviceName; 
		
		JSONGraphPair() { /* do nothing */ }
		JSONGraphPair(String graphName, String graphType, String deviceName) {
			this.deviceName = deviceName; 
			this.graphName = graphName; 
			this.graphType = graphType; 
		}
	}
		
}





