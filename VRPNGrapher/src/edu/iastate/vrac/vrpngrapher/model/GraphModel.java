package edu.iastate.vrac.vrpngrapher.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import vrpn.VRPNDevice;
import android.util.Log;
import android.widget.BaseAdapter;
import edu.iastate.vrac.vrpngrapher.GraphFactory;
import edu.iastate.vrac.vrpngrapher.VRPNConnectionManager;
import edu.iastate.vrac.vrpngrapher.graphs.ILiveChart;
import edu.iastate.vrac.vrpngrapher.model.JSONModel.JSONGraphPair;

public class GraphModel {
	
	private static ArrayList<ILiveChart> graphList = new ArrayList<ILiveChart>();
	private static Set<BaseAdapter> listeners = new HashSet<BaseAdapter>();
	
	public static List<ILiveChart> getGraphList() {
		return graphList;
	}
	
	public static void addListener(BaseAdapter listener) {
		if (listener != null)
			listeners.add(listener);
	}
	
	private static void notifyListeners() {
		for (BaseAdapter listener : listeners) {
			listener.notifyDataSetChanged();
		}
	}
	
	public static int findIndex(ILiveChart chart) {
		for (int i = 0; i < graphList.size(); i++) {
			if (chart == graphList.get(i))
				return i; 
		}
		return -1; 
	}
	
	public static ILiveChart getChart(int i) {
		return graphList.get(i); 
	}
	
	public static void addGraphs(List<JSONGraphPair> graphs, VRPNConnectionManager mgr) {
		for (JSONGraphPair p : graphs) {
			VRPNDevice dev = DeviceModel.getDevice(p.deviceName);
			if (dev == null) {
				Log.w("GraphManager", "Invalid config file. Device with name " + p.deviceName + "does not exist"); 
				continue;
			}
			addGraph(p.graphName, p.graphType, dev); 
		}
	}

	public static void addGraph(String graphName, String graphType, VRPNDevice device) {
 	   try {
			ILiveChart chart = GraphFactory.newGraph(graphName, graphType, device); 
			graphList.add(chart); 
			notifyListeners();
		} catch (ClassNotFoundException e) {
			Log.w("Graph Wizard", e); 
		} 
	}	
	
	// JSON stuff
	
	public static List<JSONGraphPair> getJsonArray() {		
		ArrayList<JSONGraphPair> al = new ArrayList<JSONModel.JSONGraphPair>(graphList.size()); 
		for (ILiveChart chart : graphList) { 
			al.add(new JSONGraphPair(chart.getName(), chart.getClass().getSimpleName(), chart.getDeviceName())); 
		}
		
		return al; 
	}

	public static String getWindowTitle() {
		return "WindowTileHere";
	}
}
