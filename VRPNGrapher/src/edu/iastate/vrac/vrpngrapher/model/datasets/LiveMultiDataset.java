package edu.iastate.vrac.vrpngrapher.model.datasets;

import java.util.ArrayList;

import org.achartengine.model.XYMultipleSeriesDataset;

import android.app.Activity;
import android.util.Log;
import edu.iastate.vrac.vrpngrapher.graphs.IChannelCountListener;
import edu.iastate.vrac.vrpngrapher.graphs.LiveGraphicalActivity;

public abstract class LiveMultiDataset extends XYMultipleSeriesDataset {

	/**
	 * generated?
	 */
	private static final long serialVersionUID = -7328055619831551063L;
	
	private ArrayList<IChannelCountListener> listeners = new ArrayList<IChannelCountListener>(); 
	private ArrayList<INewDataListener> datalisteners = new ArrayList<INewDataListener>(); 
	
	public void addChannelCountListener(IChannelCountListener listener) {
		if (listener != null)
			listeners.add(listener);
	}
	
	synchronized public void notifyChannelCountListeners(int count) {
		for (IChannelCountListener listener : listeners) {
			listener.updateChannelCount(count);
		}
	}

	public void addNewDataListener(INewDataListener listener) {
		datalisteners.add(listener);
	}
	
	synchronized protected void notifyNewDataListeners() {
		Log.v("LiveGraphicalActivity", "notifyNewDataListeners"); 
		for (INewDataListener listener : datalisteners) {
			listener.notifyNewData();
		}
	}
	
}
