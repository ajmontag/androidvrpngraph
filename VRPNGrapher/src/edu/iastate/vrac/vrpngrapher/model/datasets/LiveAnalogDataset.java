package edu.iastate.vrac.vrpngrapher.model.datasets;

import java.util.Date;

import org.achartengine.model.TimeSeries;

import edu.iastate.vrac.vrpngrapher.graphs.LiveGraphicalActivity;

import vrpn.AnalogRemote;
import vrpn.AnalogRemote.AnalogChangeListener;
import vrpn.AnalogRemote.AnalogUpdate;
import android.util.Log;

public class LiveAnalogDataset extends LiveMultiDataset implements AnalogChangeListener {
	
	private static final long serialVersionUID = 7945223086171897069L;


	@Override
	public void analogUpdate(AnalogUpdate a, AnalogRemote analog) {
		// debug console
		String s = "point [" + a.msg_time.toString() + ": ";
		for (double d : a.channel)
			s = s + d + ", ";
		Log.v("AnalogLineChart", s);
		
		UpdateRunner updateRunner = new UpdateRunner(a); 
		
//		if (uiActivity != null)
//			uiActivity.runOnUiThread(updateRunner); // shouldn't need this... should be thread safe
//		else 
			updateRunner.run();
	}
	
	
	public void addNewPoints(Date msg_time, double[] points)
	{
		if (getSeriesCount() < points.length) {
			notifyChannelCountListeners(points.length);
			// make sure there is room in the mDataset
			// this should only get entered on the first call
			for (int i = getSeriesCount(); i < points.length; i++) {
				String seriesName = "Channel " + i;
				addSeries(new TimeSeries(seriesName));
			}
		}
			
		for (int i = 0; i < points.length; i++) {
			TimeSeries series = (TimeSeries) getSeriesAt(i);
			if (series.getItemCount() > 100) // TODO dont use a literal. maybe by time?
				series.remove(0);
			series.add(msg_time, points[i]);
		}
	}
	
	
	class UpdateRunner implements Runnable {
		private AnalogUpdate a_;
		public UpdateRunner(AnalogUpdate a) {
			a_ = a; 
		}
		@Override
		public void run() {
			addNewPoints(a_.msg_time, a_.channel);
			notifyNewDataListeners();
		}
	}
}
