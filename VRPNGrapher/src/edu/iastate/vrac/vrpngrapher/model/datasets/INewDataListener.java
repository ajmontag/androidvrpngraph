package edu.iastate.vrac.vrpngrapher.model.datasets;

public interface INewDataListener {

	void notifyNewData(); 
	
}
