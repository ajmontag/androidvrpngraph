package edu.iastate.vrac.vrpngrapher;

import android.util.Log;
import vrpn.*;


public class VRPNRemoteFactory 
{

	public static VRPNDevice fromTypeName(String vrpnType, String name, 
			String localInLogfileName, String localOutLogfileName, 
			String remoteInLogfileName, String remoteOutLogfileName) 
					throws InstantiationException, ClassNotFoundException
	{
		Log.v("VRPNRemoteFactory", "Creating device from type: " + vrpnType); 
		if (0 == vrpnType.compareTo("AnalogRemote")) {
			return new AnalogRemote(name, localInLogfileName, localOutLogfileName, remoteInLogfileName, remoteOutLogfileName); 
		} else if (0 == vrpnType.compareTo("ButtonRemote")) {
			return new ButtonRemote(name, localInLogfileName, localOutLogfileName, remoteInLogfileName, remoteOutLogfileName); 
		} else if (0 == vrpnType.compareTo("ForceDeviceRemote")) {
			return new ForceDeviceRemote(name, localInLogfileName, localOutLogfileName, remoteInLogfileName, remoteOutLogfileName); 
		} else if (0 == vrpnType.compareTo("TrackerRemote")) {
			return new TrackerRemote(name, localInLogfileName, localOutLogfileName, remoteInLogfileName, remoteOutLogfileName); 
		} else {
			Log.w("VRPNRemoteFactory", "No Class with type = \"" + vrpnType + "\""); 
			throw new ClassNotFoundException("No known class with class name \"" + vrpnType + "\""); 
		}
	}
	
}
