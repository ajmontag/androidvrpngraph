package edu.iastate.vrac.vrpngrapher.graphs;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;

import vrpn.AnalogRemote;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import edu.iastate.vrac.vrpngrapher.model.GraphModel;
import edu.iastate.vrac.vrpngrapher.model.datasets.INewDataListener;
import edu.iastate.vrac.vrpngrapher.model.datasets.LiveAnalogDataset;

@SuppressLint("HandlerLeak")
public class AnalogLineGraph extends ALiveChart {

	private String name_;
	
	private LiveAnalogDataset mDataset = new LiveAnalogDataset(); // TODO really separate these
	
	private AnalogRemote remote_;
	private GraphicalView view_;
	
	public AnalogLineGraph(String name, AnalogRemote remote)
	{
		remote_ = remote; 
		name_ = name; 
		
		final int chCount = remote.getNumActiveChannels();
		Log.v("AnalogLineGraph", "chCount = " + chCount);
		
		// Enable Zoom
		mRenderer.setZoomButtonsVisible(true);
		mRenderer.setXTitle("time");
		mRenderer.setYTitle("Analog");
		
		// TODO need to make sure there are enough renderers for channels
		
		remote.addAnalogChangeListener(mDataset);
		mDataset.addChannelCountListener(this); 
	}
	
	// MUST be called from UI thread
	@Override
	public GraphicalView getView(Context context, INewDataListener listener) 
	{
		mDataset.addNewDataListener(listener); 
		if (view_ == null) {
			Log.v("AnalogLineGraph", "view is null, creating new chart"); 
			view_ = ChartFactory.getLineChartView(context, mDataset, mRenderer);
			view_.setBackgroundColor(Color.DKGRAY);
		}
		return view_; 
	}
	
	@Override 
	public String toString() {
		return getName(); 
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name_;
	}

	@Override
	public String getDesc() {
		return "Description";
	}

	@Override
	public Intent execute(Context context) {
		Intent intent = new Intent(context, LiveGraphicalActivity.class);
		intent.putExtra("chartIndex", GraphModel.findIndex(this));
//	    XYChart chart = new TimeChart(mDataset, mRenderer);
//	    intent.putExtra(ChartFactory.CHART, chart);
//	    intent.putExtra(ChartFactory.TITLE, "title-goes-here");
	    return intent;
		//return null; 
	}

	@Override
	public String getDeviceName() {
		return remote_.toString();
	}
	
}
