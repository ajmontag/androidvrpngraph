/**
 * Copyright (C) 2009, 2010 SC 4ViewSoft SRL
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.iastate.vrac.vrpngrapher.graphs;

import org.achartengine.GraphicalView;

import edu.iastate.vrac.vrpngrapher.model.datasets.INewDataListener;

import android.content.Context;
import android.content.Intent;

public interface ILiveChart {

  /**
   * Returns the chart name.
   * @return the chart name
   */
  String getName();

  /**
   * Returns the chart description.
   * @return the chart description 
   */
  String getDesc();

  /**
   * Executes the chart demo.
   * 
   * @param context the context
   * @return the built intent
   */
  Intent execute(Context context);
  
  //GraphicalView getView(Context context);

  String getDeviceName();

  GraphicalView getView(Context context, INewDataListener listener);
}
