package edu.iastate.vrac.vrpngrapher.graphs;

import org.achartengine.GraphicalView;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import edu.iastate.vrac.vrpngrapher.model.GraphModel;
import edu.iastate.vrac.vrpngrapher.model.datasets.INewDataListener;

public class LiveGraphicalActivity extends Activity implements INewDataListener {

	  private GraphicalView mView;

	@Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    /*
	    Bundle extras = getIntent().getExtras();
	    mChart = (AbstractChart) extras.getSerializable(ChartFactory.CHART);
	    mView = new GraphicalView(this, mChart);
	    String title = extras.getString(ChartFactory.TITLE); */
	    /*if (title == null) {
	      requestWindowFeature(Window.FEATURE_NO_TITLE);
	    } else if (title.length() > 0) {
	      setTitle(title);
	    }*/
	    Bundle extras = getIntent().getExtras();
	    int index = extras.getInt("chartIndex", -1);
	    Log.v("LiveGraphicalActivity", "index = " + index);
	    final ILiveChart chart = GraphModel.getChart(index);
	    setTitle(chart.toString());
	    
	    runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Log.v("LiveGraphicalActivity", "creating new view"); 
			    mView = chart.getView(getBaseContext(), LiveGraphicalActivity.this); // MUST call this from UI thread
			    setContentView(mView);
			}
		});
	  }

	@Override
	public void notifyNewData() {
		Log.v("LiveGraphicalActivity", "notifyNewData"); 
		mView.repaint(); 
	}
}
