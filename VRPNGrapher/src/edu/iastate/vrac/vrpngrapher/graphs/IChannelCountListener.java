package edu.iastate.vrac.vrpngrapher.graphs;

public interface IChannelCountListener {
	void updateChannelCount(int count);
}
