package edu.iastate.vrac.vrpngrapher;

//import android.app.ListFragment;
//import android.app.Fragment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;

import org.achartengine.GraphicalView;

import vrpn.ButtonRemote;
import vrpn.ButtonRemote.ButtonUpdate;
import vrpn.VRPNDevice;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.gson.Gson;

import edu.iastate.vrac.vrpngrapher.graphs.ILiveChart;
import edu.iastate.vrac.vrpngrapher.model.DeviceModel;
import edu.iastate.vrac.vrpngrapher.model.GraphModel;
import edu.iastate.vrac.vrpngrapher.model.JSONModel;

public class MainActivity extends FragmentActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
     * sections. We use a {@link android.support.v4.app.FragmentPagerAdapter} derivative, which will
     * keep every loaded fragment in memory. If this becomes too memory intensive, it may be best
     * to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

	private static VRPNConnectionManager connectionManager_;
	private static GraphManager graphManager_;
	//private GraphicalView graphView_;

    public class ButtonTest implements vrpn.ButtonRemote.ButtonChangeListener
	{
		@Override
		public void buttonUpdate(ButtonUpdate u, ButtonRemote button) {
			// TODO Auto-generated method stub
			Log.v("Button0", button.toString() + " -- " + u.toString());
		}
	}
	
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.v("MainActivity", "onCreateBundle Top");
        
        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        connectionManager_ = new VRPNConnectionManager(this); 
        graphManager_ = new GraphManager(this); 

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        
        /* TESTING VRPN STUFF
        ButtonRemote button0 = null;
        try {
        	button0 = new ButtonRemote("button0@192.168.1.108", "", "", "", ""); 
        	ButtonChangeListener changeListener = new ButtonTest();
			button0.addButtonChangeListener(changeListener );
			Log.v("MainActivity", "initiailized button: " + button0.toString()); 
        } catch (InstantiationException e) {
        	Log.e("MainActivity", e.toString());
        }

        if (button0 != null) {
        	Log.v("MainActivity", "Connected: " + button0.isConnected());
        } else {
        	Log.w("MainActivity", "button0 is null!");
        } */
        
        Log.v("MainActivity", "onCreateBundle Bottom");
    }
    
    /*    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.v("MainActivity", "BACK Pressed");
            if (graphView_ != null) {
            	Log.v("MainActivity", "Removing Graph View");
            	ViewGroup vg = (ViewGroup)(graphView_.getParent());
            	vg.removeView(graphView_);
            	graphView_ = null; 
            	return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    } */
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected (MenuItem item)
    {
    	switch(item.getItemId()) {
    	case R.id.new_vprn_connection:
    		connectionManager_.promptNewConnection(); 
    		return true;
    	case R.id.load:
    		onLoad(); 
    		return true; 
    	case R.id.save:
    		onSave(); 
    		return true; 
    	}
    	return false; 
    }


    private void onLoad() {
    	Log.v("Main Activity", "Load requested"); 
		
    	// TODO prompt them for the file
		File file = new File(getExternalFilesDir(null), "GraphVRPN.json");
		
		Gson gson = new Gson();
		
		JSONModel jc; 
		try {
			Reader r = new FileReader(file);
			jc = gson.fromJson(r, JSONModel.class); 
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return; 
		}   	
		if (null == jc) {
			// TODO error
			Log.e("onLoad", "error deserializing to JSON"); 
			return; 
		}
		DeviceModel.connectAndAdd(jc.connections, true); 
		GraphModel.addGraphs(jc.graphs, connectionManager_); 
	}

	private void onSave() {
		Log.v("Main Activity", "Save requested");
		
		Gson gson = new Gson();  
		JSONModel jc = new JSONModel(this); 
		String json = gson.toJson(jc); 
		System.out.println(json);
		
		// TODO ask them where to save it
		
		
		File file = new File(getExternalFilesDir(null), "GraphVRPN.json");
		try {
			OutputStream os = new FileOutputStream(file);
			os.write(json.getBytes()); 
		} catch (FileNotFoundException e) {
			Toast.makeText(getBaseContext(), "File Not Found", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		} catch (IOException e) {
			Toast.makeText(getBaseContext(), "Error Writing to File", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	/**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {


		public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            Log.v("MainActivity", "SectionsPagerAdapter");
        }

        @Override
        public Fragment getItem(int i) {
            ListFragment fragment = new ListFragment();
            //Bundle args = new Bundle();
            //args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
            //fragment.setArguments(args);
            
            String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
    		  "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
    		  "Linux", "OS/2" };

    		ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
    		  android.R.layout.simple_list_item_1, android.R.id.text1, values);

    		// Assign adapter to ListView
    		fragment.setListAdapter(adapter); 
        	
    		ListFragment lf = null; 
        	switch (i) {
        	case 0:
        		lf = new VRPNConnectionListFragment(); 
        		lf.setListAdapter(connectionManager_.getListAdapter());
        		return lf; 
        	case 1: 
        		lf = new GraphListFragment(); 
        		lf.setListAdapter(graphManager_.getListAdapter());
        		return lf; 
        	case 2: 
        		return fragment; 
        	default:
        		return null; 
        	}
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: return getString(R.string.title_vrpn_devices).toUpperCase();
                case 1: return getString(R.string.title_graphs).toUpperCase();
                case 2: return getString(R.string.title_dashboards).toUpperCase();
            }
            return null;
        }
    } // end class SectionsPagerAdapter

    
    static public class VRPNConnectionListFragment extends ListFragment {
    	public void onActivityCreated (Bundle savedInstanceState) {
    		super.onActivityCreated(savedInstanceState); 
    		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
					Log.v("VRPNConnectionListFragment", "OnLongClick position:" + position); 
					VRPNDevice dev = connectionManager_.getDevice(position); 
					if (dev == null)
						return false; 
					graphManager_.promptNewGraph(dev); 
					return true; 
				}
			});
    	}
    } // end class VRPNConnectionListFragment
    
    static public class GraphListFragment extends ListFragment {
    	public void onActivityCreated (Bundle savedInstanceState) {
    		Log.v("GraphListFragment", "onActivityCreated"); 
    		super.onActivityCreated(savedInstanceState); 
    		getListView().setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					Log.v("GraphListFragment", "OnClick position:" + position); 
					ILiveChart chart = graphManager_.getGraph(position); 
					if (chart == null) {
						Log.w("GraphListFragment", "Null graph"); 
						return; 
					}
					Log.v("GraphListFragment", "about to execute graph " + chart); 
					startActivity(chart.execute(getActivity().getBaseContext()));
					//graphView_ = chart.getView(getBaseContext());
					//LayoutParams layoutP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
					//addContentView(graphView_, layoutP);
				}
			});
    	}
    } // end class VRPNConnectionListFragment
}
