package edu.iastate.vrac.vrpngrapher;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import edu.iastate.vrac.vrpngrapher.model.DeviceModel;

import vrpn.VRPNDevice;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class VRPNConnectionManager {
	

	
	ArrayAdapter<VRPNDevice> arrAdapter_; 
	Activity activity_; 
	
	public VRPNConnectionManager(Activity activity) {
		activity_ = activity; 
		arrAdapter_ = new ArrayAdapter<VRPNDevice>(activity_, android.R.layout.simple_list_item_1, DeviceModel.getDeviceList());
		DeviceModel.addListener(arrAdapter_);
	}
	
	public void promptNewConnection() {
		new NewConnectionWizard(activity_);
	}
	
	ArrayAdapter<VRPNDevice> getListAdapter() {
		return arrAdapter_; 
	}
	
	public VRPNDevice getDevice(int i) {
		return arrAdapter_.getItem(i); 
	}
	
	
	
	
	private class NewConnectionWizard {
		
		Dialog dialog_; 
		EditText et_;
		protected CharSequence type = "";
		
		public NewConnectionWizard(final Activity activity) {
			// TODO Auto-generated constructor stub
						
			View view = activity.getLayoutInflater().inflate(R.layout.dialog_new_vprn_connecton, null); 
			ListView lv = (ListView) view.findViewById(R.id.vrpn_connection_types_list_view);
			if (lv == null) {
				throw new RuntimeException("view by id not found"); 
			}

			lv.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					TextView tv = (TextView) arg1;
					type = tv.getText();
					tv.setAlpha(0.5f);
				}
			});
			
			et_ = (EditText) view.findViewById(R.id.device_location); 
			if (et_ == null) {
				throw new RuntimeException("view by id not found"); 
			}			
			
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		    // Set the dialog title
		    builder.setTitle("New VRPN Connection")
		    .setView(view)
		    // Set the action buttons
		           .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {
		            	   dialog_.dismiss();
		            	   Log.v("Connection Wizard", "Ok. Type[" + type + "], Location[" + et_.getText() + "]");
		            	   DeviceModel.connectAndAdd(et_.getText().toString(), type.toString(), false); 
		               }
		           })
		           .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {
		                   NewConnectionWizard.this.dialog_.cancel(); 
		                   type = ""; 
		               }
		           });
		    dialog_ = builder.create();
		    dialog_.show(); 
		}
	}	
}
