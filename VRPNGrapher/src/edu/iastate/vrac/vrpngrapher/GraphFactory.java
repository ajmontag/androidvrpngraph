package edu.iastate.vrac.vrpngrapher;

import android.app.Activity;
import android.util.Log;
import vrpn.AnalogRemote;
import vrpn.VRPNDevice;
import edu.iastate.vrac.vrpngrapher.graphs.AnalogLineGraph;
import edu.iastate.vrac.vrpngrapher.graphs.ILiveChart;
import edu.iastate.vrac.vrpngrapher.graphs.TrigonometricFunctionsChart;

public class GraphFactory {

	public static ILiveChart newGraph(String graphName, String graphClassName, VRPNDevice device) throws ClassNotFoundException {
		if (graphClassName.equals(TrigonometricFunctionsChart.class.getSimpleName())) {
			return new TrigonometricFunctionsChart(graphName); 
		} else if (graphClassName.equals(AnalogLineGraph.class.getSimpleName())) {
			if (device == null) Log.w("GraphFactory", "device is null!");
			if (AnalogRemote.class != device.getClass()) throw new ClassNotFoundException("VPRNDevice must be of type AnalogRemote!");
			return new AnalogLineGraph(graphName, (AnalogRemote) device); 
		} else {
			throw new ClassNotFoundException("Unknown graph type" + graphClassName); 
		}
	}
}
